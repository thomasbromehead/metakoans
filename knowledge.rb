puts "I've been required!"

module Knowledge
    def self.callbacks
        []
    end
    module Koan1
        def say_hi
            puts "saying hi!"
        end

        def self.extended(mod)
            puts "#{self} extended in #{mod}"
        end

        def initialize(*args)
            self.class.class_variables.each do |variable|
                value = self.class.class_variable_get(variable)
                method = variable.to_s[2..-1]
                if Proc === value
                    self.send(:"#{method}=", instance_exec(&value))
                else
                    self.send(:"#{method}=", value)
                end
            end
            # value ||= self.class.class_variable_get(:"@@#{method_name}") rescue nil
            # if Proc === value
            #     instance_variable_set(:"@#{argument}", self.instance_exec(&value))
            # else
            #     instance_variable_set(:"@#{argument}", value)
            # end
            !args.empty? ? super(args) : super()
        end

        def self.included(base)
            # Instance Eval keeps self pointing to the class instead of the metaclass

            base.class_eval{
                # Methods are available on the instances
                def hello_from_class
                    puts "hello from class"
                end
            }

            def this_method
                puts "this method"
            end

            if base.singleton_class? && base.instance_of?(Object)
                # binding.irb if base.instance_of?(Class)
                binding.irb
                base.send(:define_singleton_method, :attribute) do |*args|
                        # Define singleton / eigenclass methods
                        arg = args.first
                        self.send(:define_singleton_method, :"#{arg}?") do 
                            instance_variable_defined?(:"@#{arg}")
                        end
                        self.send(:define_singleton_method, :"#{arg}=") do |args|
                            instance_variable_set(:"@#{arg}", args)
                        end
                        self.send(:define_singleton_method, arg.to_sym) do
                            instance_variable_get(:"@#{arg}")
                        end
                end
            else
                base.send(:define_singleton_method, :attribute) do |*args, &block|
                        binding.irb
                        # Define instance methods
                        argument = args.first
                        # You cannot set instance or class variables from the metaclass, you have to use instance_eval to keep self pointing to the class
                        if argument.is_a?(Hash)
                            arg, value = argument.keys[0], argument.values[0]
                            self.send(:define_method, :"#{arg}?") do 
                                !!instance_variable_get(:"@#{arg}")
                            end
                            self.send(:define_method, :"#{arg}=") do |value|
                                instance_variable_set(:"@#{arg}", value)
                            end
                            self.send(:define_method, :"#{arg}") do
                                instance_variable_get(:"@#{arg}") || instance_variable_set(:"@#{arg}", nil)
                            end
                            base.class_variable_set(:"@@#{arg}", value)
                            binding.irb
                            if base.singleton_class?
                                binding.irb
                                def atchum;;end
                                base.public_instance_methods << :atchum
                            end
                            # Create a fiber here that will be called afterwards
                        elsif !!block
                            # begin
                                self.send(:define_method, :"#{argument}?") do 
                                    !!instance_variable_get(:"@#{argument}")
                                end
                                self.send(:define_method, :"#{argument}=") do |value|
                                    instance_variable_set(:"@#{argument}", value)
                                end
                                self.send(:define_method, :"#{argument}") do
                                    instance_variable_get(:"@#{argument}") || instance_variable_set(:"@#{argument}", nil)
                                end
                                base.class_variable_set(:"@@#{argument}",  block)
                            # rescue
                            #     method_name = $!.name.to_s
                            #     # Class variable set to method's name will hold a reference to the block, later called 
                            #     # as instance_exec(&block)
                            #     value = block
                            #     # value = self.new.instance_eval(&block) rescue nil
                            #     if value.nil?
                            #         value = block
                            #     else
                            #     end
                            # end
                            # if base.public_methods.include?(:attribute)
                            #     # No need to redefine :attribute if it's already been defined
                            #     binding.irb
                            #     send(:attribute, arg, block)
                            # end
                        else
                            arg = argument
                            self.send(:define_method, :"#{arg}?") do 
                                !!instance_variable_get(:"@#{arg}")
                            end
                            self.send(:define_method, :"#{arg}=") do |value|
                                instance_variable_set(:"@#{arg}", value)
                            end
                            self.send(:define_method, :"#{arg}") do
                                instance_variable_get(:"@#{arg}") || instance_variable_set(:"@#{arg}", nil)
                            end
                            base.instance_variable_set(:"@@#{arg}", nil)
                        end
                end
            end
        end

        # You would write this method here then extend the module but it would then also be available for instances.
        # Prefer the approach above OR scope methods Knowledge::Koans1::ClassMethods/InstanceMethods
        # def attribute arg
        #     binding.irb
        # end
    end
end

# Try using AT_EXIT HOOK
# module MetaKoans
#     my_rescue = lambda{ puts "rescued"}
      
#       at_exit do
#         binding.irb
#         puts $ERROR_INFO 
#         my_rescue.call()
#       end
      
# end